package ru.rencredit.jschool.kuzyushin.tm.test.repository;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.marker.UnitCategory;
import ru.rencredit.jschool.kuzyushin.tm.repository.UserRepository;

import java.util.List;

public class UserRepositoryTest {

//    private final IUserRepository userRepository = new UserRepository();
//
//    final private static User testUser = new User();
//
//    @BeforeClass
//    public static void initUser() {
//        testUser.setLogin("userOne");
//        testUser.setId("123123");
//    }
//
//    @Before
//    public void addUser() {
//        userRepository.add(testUser);
//    }
//
//    @After
//    public void deleteUser() {
//        userRepository.clear();
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void addTest() {
//        userRepository.add(testUser);
//        final User user = userRepository.findById(testUser.getId());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(user.getLogin(), testUser.getLogin());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void removeTest() {
//        final User user = userRepository.findById(testUser.getId());
//        Assert.assertNotNull(user);
//        userRepository.remove(user);
//        Assert.assertEquals(0, userRepository.findAll().size());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void findAllTest() {
//        final List<User> users = userRepository.findAll();
//        Assert.assertNotNull(users);
//        Assert.assertEquals(users.size(), 1);
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void findByIdTest() {
//        final User user = userRepository.findById(testUser.getId());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(user.getLogin(), testUser.getLogin());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void findByLoginTest() {
//        final User user = userRepository.findByLogin(testUser.getLogin());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(user.getLogin(), testUser.getLogin());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void removeByIdTest() {
//        final User user = userRepository.findById(testUser.getId());
//        Assert.assertNotNull(user);
//        userRepository.removeById(user.getId());
//        Assert.assertNull(userRepository.findById(testUser.getId()));
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void removeByLoginTest() {
//        final User user = userRepository.findById(testUser.getId());
//        Assert.assertNotNull(user);
//        userRepository.removeByLogin(user.getLogin());
//        Assert.assertNull(userRepository.findById(testUser.getId()));
//    }
}
