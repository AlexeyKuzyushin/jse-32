package ru.rencredit.jschool.kuzyushin.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.rencredit.jschool.kuzyushin.tm")
public class ServerConfiguration {

}
