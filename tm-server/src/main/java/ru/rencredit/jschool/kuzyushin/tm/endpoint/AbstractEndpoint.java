package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;

public abstract class AbstractEndpoint {

    @Nullable
    protected ISessionService sessionService;

    @Autowired
    public AbstractEndpoint(final @NotNull ISessionService sessionService) {
        this.sessionService = sessionService;
    }
}
