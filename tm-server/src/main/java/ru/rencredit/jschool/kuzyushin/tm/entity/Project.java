package ru.rencredit.jschool.kuzyushin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
public class Project extends AbstractEntity {

    @NotNull
    @Column(columnDefinition = "TINYTEXT",
            nullable = false)
    private String name;

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description;

    @NotNull
    @ManyToOne
    private User user;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    @Column(updatable = false)
    private Date creationTime = new Date(System.currentTimeMillis());

    @NotNull
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();
}
