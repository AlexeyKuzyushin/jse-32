package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository <E extends AbstractEntity> implements IRepository<E> {

    protected EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void remove(@NotNull E e) {
        entityManager.remove(e);
    }

    @Override
    public void persist(@NotNull final E e) {
        entityManager.persist(e);
    }

    @Override
    public void merge(@NotNull final E e) {
        entityManager.merge(e);
    }

}
