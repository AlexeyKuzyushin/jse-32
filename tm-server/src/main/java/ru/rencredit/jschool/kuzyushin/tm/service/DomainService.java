package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;

@Service
public class DomainService implements IDomainService {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    public DomainService(final @NotNull ITaskService taskService,
                         final @NotNull IProjectService projectService,
                         final @NotNull IUserService userService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setTasks(taskService.findAll());
        domain.setProjects(projectService.findAll());
        domain.setUsers(userService.findAll());
    }
}
