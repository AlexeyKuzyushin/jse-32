package ru.rencredit.jschool.kuzyushin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class Domain implements Serializable {

    @NotNull
    private List<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    private List<TaskDTO> tasks = new ArrayList<>();

    @NotNull
    private List<UserDTO> users = new ArrayList<>();
}
