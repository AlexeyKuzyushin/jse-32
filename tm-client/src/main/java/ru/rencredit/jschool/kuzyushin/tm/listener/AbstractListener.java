package ru.rencredit.jschool.kuzyushin.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListener {

    @Nullable
    protected ISessionService sessionService;

    @Autowired
    public AbstractListener(final @NotNull ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    public AbstractListener() {

    }

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String description();

    public abstract void handler(final ConsoleEvent event) throws Exception;
}
