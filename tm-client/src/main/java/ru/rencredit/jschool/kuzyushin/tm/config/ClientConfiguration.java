package ru.rencredit.jschool.kuzyushin.tm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.*;

@Configuration
@ComponentScan(basePackages = "ru.rencredit.jschool.kuzyushin.tm")
public class ClientConfiguration {

    @Bean
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    public SessionEndpoint sessionEndpoint() {
        return sessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    public DataEndpointService dataEndpointService() {
        return new DataEndpointService();
    }

    @Bean
    public DataEndpoint dataEndpoint() {
        return dataEndpointService().getDataEndpointPort();
    }

    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    public TaskEndpoint taskEndpoint() {
        return taskEndpointService().getTaskEndpointPort();
    }

    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    public ProjectEndpoint projectEndpoint() {
        return projectEndpointService().getProjectEndpointPort();
    }

    @Bean
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    public UserEndpoint userEndpoint() {
        return userEndpointService().getUserEndpointPort();
    }
}
