package ru.rencredit.jschool.kuzyushin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByNameListener extends AbstractListener {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @Autowired
    public TaskRemoveByNameListener(
            final @NotNull TaskEndpoint taskEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name";
    }

    @Override
    @EventListener(condition = "@taskRemoveByNameListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        taskEndpoint.removeTaskByName(sessionDTO, name);
        System.out.println("[OK]");
    }
}
