package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

@Service
public class SessionService implements ISessionService {

    @Nullable
    private SessionDTO currentSession;

    @Nullable
    @Override
    public SessionDTO getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(final SessionDTO currentSession) {
        this.currentSession = currentSession;
    }

    @Override
    public void clearCurrentSession() {
        currentSession = null;
    }
}
