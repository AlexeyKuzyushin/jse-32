package ru.rencredit.jschool.kuzyushin.tm.listener.data.json;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.DataEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

@Component
public class DataJsonSaveListener extends AbstractListener {

    @NotNull
    private final DataEndpoint dataEndpoint;

    @Autowired
    public DataJsonSaveListener(
            final @NotNull DataEndpoint dataEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.dataEndpoint = dataEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to json file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonSaveListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA JSON SAVE]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        dataEndpoint.saveDataJson(sessionDTO);
        System.out.println("[OK]");
    }
}
